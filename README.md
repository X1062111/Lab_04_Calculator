# Software Studio 2018 Spring Lab 04 Calculator

## Goal
1. Fork this repo. (appearence template)
2. Complete the web calculator.
    * Display input sequence on calculator’s screen, and when a “=” is clicked, show the evaluate result, .
    * If the input sequence is an illegal argument, show the error message. (ex. 3+/5)
3. **Deadline: 2018/03/27 17:20 (commit time)**
    * Delay will get 0 point, and do not discuss to each other.
    * **Deploy to Gitlab page after 17:20**

## Hint
1. Use "onclick" event handler to pass value to JavaScript.
2. Use "getElementbyID" to get input text element. 
3. **Make use of "eval()" function.**
4. The "try{} catch{}" statements can help you easily handle illegal argument.
    * What does eval() function return, when it is illegal?


problem:
1   为什么使用   .document.getElementById("screen").value="0"  
清零后，其他click事件没有反应
而使用函数（如下）就可以？
function backzero()
{
    var origion= document.getElementById("screen");
    origion.getAttributeNode("value").value="0";
}
2   为什么不能跟之前网页一样直接打开，而是需要加主页名才能打开
3   为什么js代码以外部文件形式存储时，打开网页后，js代码不会被加载，而将js代码写在html文件内部就可以正常运行？